import '../public/global.css';
import Layout from '../components/Layout';

function MyApp({ Component, pageProps }) {
    return (
        <Layout meta={pageProps.meta}>
            <Component {...pageProps} />
        </Layout>
    );
};

export default MyApp;
