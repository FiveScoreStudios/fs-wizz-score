export default function Home(props) {
    return (
        <div id="homeContent">
            <p>Welcome to my app!</p>
        </div>
    )
}

export async function getStaticProps() {
    const meta = {
        title: 'My App',
        description: 'My fancy app',
        author: 'Five Score Studios',
        keywords: '',
    }

    return {
        props: { meta },
    }
}
