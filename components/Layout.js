import Header from './Header'
import Footer from './Footer'
import MetaData from './MetaData'

export default function Layout(props) {
    return (
        <>
            {/* <MetaData data={props.meta}></MetaData> */}
            <Header></Header>

            <main>
                <div>{props.children}</div>
            </main>

            <Footer></Footer>
        </>
    )
}
