export default function Footer(props) {
    return (
        <footer>
            <p>&copy; Five Score Studios 2022</p>
        </footer>
    )
}
